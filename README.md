# BoB schema bob-authz-groups

BoB uses [JWT claims](https://datatracker.ietf.org/doc/html/rfc7519#section-4) for authentication tokens. See [BoB Authentication API](https://samtrafiken.atlassian.net/wiki/spaces/BOB/pages/342262084/BoB+Authentication+OpenAPI).

The private claim _bobAuthZ_ identifies the BoB AuthZ group of the JWT holder and the use of this claim is REQUIRED. Its value must be a string, and it is RECOMMENDED to use the values in [bob-authz-groups.json](https://bitbucket.org/samtrafiken/bob-schema-bob-authz-groups/src/master/bob-authz-groups.json).

This is a part of the [BoB - National Ticket and Payment Standard](https://bob.samtrafiken.se), hosted by Samtrafiken i Sverige AB